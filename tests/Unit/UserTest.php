<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    public function test_user_add()
    {
        $data = [
            "data"=>[
                [
                 "first_name"=>"cody",
                  "last_name"=>"duder",
                  "age"=>1000,
                  "email"=>"codyduder@causelabs.com ",
                  "secret"=>"VXNlIHRoaXMgc2VjcmV0IHBocmFzZSBzb21ld2hlcmUgaW4geW91ciBjb2RlJ3MgY29tbWVudHM="
                ]
            ]
        ];
        
        $response = $this->json('POST', 'api/user/add', $data);

        $response->assertStatus(201);
    }

    public function test_failed_user_add()
    {
        $data = [
                [
                 "first_name"=>"cody",
                  "last_name"=>"duder",
                  "age"=>1000,
                  "email"=>"codyduder@causelabs.com ",
                  "secret"=>"VXNlIHRoaXMgc2VjcmV0IHBocmFzZSBzb21ld2hlcmUgaW4geW91ciBjb2RlJ3MgY29tbWVudHM="
                ]
        ];
        
        $response = $this->json('POST', 'api/user/add', $data);

        $response->assertStatus(422);
    }
}
