<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;

class UserController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->data) {
            return response()->json(['error'=> 'check the formart'], 422);
        }
        $validator = Validator::make($request->all(), [
            'data.*.first_name' => 'required',
            'data.*.last_name' => 'required',
            'data.*.age' => 'required',
            'data.*.secret' => 'required',
            'data.*.email' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=> $validator->errors()], 422);
        }

        $collection = (collect($request->data));

        $emailList = $collection->pluck('email')->all();

        $changed = $collection->map(function ($value, $key) {
            $value['name'] = $value['first_name']." ".$value['last_name'];
            return $value;
        });
     
        $sortedRequest = $changed->sortBy('age')->values()->all();

        $data =collect([]);
        for ($i=0; $i<count($sortedRequest); $i++) {
            $user = new User;
            $user->name = $sortedRequest[$i]['name'];
            $user->email = $sortedRequest[$i]['email'];
            $user->secret = $sortedRequest[$i]['secret'];
            $user->first_name = $sortedRequest[$i]['first_name'];
            $user->last_name = $sortedRequest[$i]['last_name'];
            $user->age = $sortedRequest[$i]['age'];
            $user->ip_address = $request->ip();
            $user->email_list = ($emailList);
            $user->save();
            $data->push($user);
        }

        return response()->json($data, 201);
    }
}
